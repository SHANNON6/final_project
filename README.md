# Final Project

This Karissa Shannon's final project for AOS 573. My goal for this project was to explore snowfall data in the polar regions while applying tools we used in class.

## Files in this Repository
monthly_c3s: folder containing monthly snowfall data from the C3S satellite product. 
aws_stations.csv: csv with aws station metadata
final.ipynb: Notebook containing all the code for this project
aos573.yml: yml file for the environment used
README.me: what you are reading right now!

## Monthly_C3S files
These .nc file contain gridded snowfall data. The naming structure is as follows: fixed_snowfall_YEARMONTH.nc. These files were created by Marian Mateling and are not yet publicly available. More information about these files can be found in the jupyter notebook.

## aws_stations
I created this file on my personal computer because of the limits of the environment I used in this notebook. It contains metadata information for the AWS stations. This information was found at [this link](http://amrc.ssec.wisc.edu/aws/station_list.php). More information about this file can be found in the jupyter notebook.

## Additional Data
Additional data was used from [this website](https://amrdcdata.ssec.wisc.edu/). This data can be directly read in from the notebook, so it's not necessary to download. More information about this data can be found in the jupyter notebook.

## aos573.yml
A yml file for the environment used in the jupyter notebook. This is the same as the AOS 573 environment in the aos jupyter hub. 

## final.ipynb
This jupyter notebook contains all the python code for this project. The notebook contains text that explains the steps taken as well as some commented code explaining specific lines of code. The code is designed to run in order from top to bottom with no additional steps needed.

## Authors and acknowledgment
This code was written by Karissa Shannon. Thank you to Marian Mateling and Tristan L'ecuyer for the work on the C3S satellite data. And thanks to the AMRDC for the accessable data on their website. Also thanks to Hannah Zanowski for teaching me how to write the code ;) 

